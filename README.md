**Repository location moved! Please check [https://github.com/MaastrichtU-CDS/fair_tools_docker-graphdb](https://github.com/MaastrichtU-CDS/fair_tools_docker-graphdb)**
# GraphDB Docker

## How to run?
```
docker run -d \
    --name graphdb \
    -p 7200:7200 \
    --restart unless-stopped \
    registry.gitlab.com/um-cds/fair/tools/docker-graphdb:latest
```